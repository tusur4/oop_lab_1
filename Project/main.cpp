/**
* This program create an adress book.
* @ version 1.1 20.07.2023.
* @ autor Pavel Lyubogradov.
* Copyright 2023, Pavel Lyubogradov, All rights reserved.
*/

#include <cctype>
#include <algorithm>
#include "my_vector.h"

vector <class Address*> d_array;

/*
* To enter data into the program.
*/
void input(void);

/*
* To search an entry into the program.
*/
void search(void);

/*
* To update an entry into the program.
*/
void update(void);

/*
* To delete an entry into the program.
*/
void delete_contact(void);

/*
* Print the address book.
*/
void print(void);

/*
* To dump data to the file.
*/
void dump(void);

/*
* Restore data to the file.
*/
void restore(void);

/*
* Get the file path
* @return The file path
*/
char* file_path(void);


/*
* Testing created clases
*/
void test(void);


int main(void)
{
	bool extit_from_loop;
	char code;

	test();
	cout << "\n\n\n";

	cout << "\n ************** THE ADDRESS BOOK **************\n ";

	for (extit_from_loop = false; !extit_from_loop;)
	{
		cout << "Codes:\ni - input; s - search; u - update;d - dump; t - terminte contact; r - restore; p - print; q - exit.\n\n";
		cout << "Enter operation code: ";

		if (scanf_s(" %c", &code, (unsigned)sizeof(code)) <= 0)
			continue;
		while (getchar() != '\n') /* skips to end of line*/
			;
		switch (code)
		{
		case 'i':
			input();
			break;
		case 's':
			search();
			break;
		case 'u':
			update();
			break;
		case 'd':
			dump();
			break;
		case 't':
			delete_contact();
			break;
		case 'r':
			restore();
			break;
		case 'p':
			print();
			break;
		case 'q':
			extit_from_loop = true;
			break;
		default:
			cout << ("Illegal code\n");
		}
		cout << ("\n");
	}
	d_array.clear();
	d_array.shrink_to_fit();

	return 0;
}

void input(void)
{
	Address* address = new Address();

	address->input(true, d_array);

	d_array.push_back(address);

}

void search(void)
{
	int number;
	int index;

	cout << "Enter person number: ";
	number = my_get_int();
	index = seach_address_in_vector_by_number(d_array, number);

	if (index >= 0)
	{
		cout << "\n ****************************\n ";
		d_array[index]->print();
		cout << "\n ****************************\n ";
	}
	else
		cout << "A person not found.\n";
}

void update(void)
{
	int number;
	int index;
	Address* address;

	cout << "Enter person number: ";
	number = my_get_int();

	index = seach_address_in_vector_by_number(d_array, number);

	if (index >= 0)
	{
		cout << "\nThe found element \n<<*";
		address = d_array[index];
		address->print();
		cout << "\n*>>\n";
		address->input(false, d_array);
		cout << "\nThe new element \n<< *";
		address->print();
		cout << ("\n*>>\n");
	}
	else
		cout << "A person not found.\n";
}


void delete_contact(void)
{
	int number;
	int index;

	cout << "Enter person number: ";
	number = my_get_int();
	index = seach_address_in_vector_by_number(d_array, number);
	if (index >= 0)
	{
		d_array.erase(d_array.begin() + index);
		d_array.shrink_to_fit();
		cout << "\nDeleted successfully\n";
	}
	else
		cout << "An element not found\n";
}

void dump(void)
{
	char* path;
	int  i;
	size_t total_of_obj;
	size_t size;
	FILE* stream;

	size = d_array.size();

	if ((path = file_path()) == NULL)
		return;

	if (fopen_s(&stream, path, "wb"))
	{
		fprintf(stderr, "ERROR: fopen_s %s", path);
		exit(EXIT_FAILURE);
	}

	total_of_obj = 1;
	if ((fwrite(&size, (size_t)sizeof(size_t), (size_t)total_of_obj, stream)) != total_of_obj)
	{
		fprintf(stderr, "ERROR: fwrite %s", path);
		fclose(stream);
		exit(EXIT_FAILURE);
	}

	total_of_obj = size;
	for (i = 0; i < total_of_obj; i++)
	{
		if ((fwrite(d_array[i], (size_t)sizeof(Address), 1, stream)) != 1)
		{
			fprintf(stderr, "ERROR: fwrite %s", path);
			fclose(stream);
			exit(EXIT_FAILURE);
		}
	}

	free(path);
	fclose(stream);

}

void restore(void)
{
	char* path;
	size_t total_of_obj;
	long int offset;
	FILE* stream;
	errno_t err;
	Address* address;

	d_array.clear();

	if ((path = file_path()) == NULL)
		return;

	if (fopen_s(&stream, path, "rb"))
	{
		fprintf(stderr, "ERROR: fopen_s %s", path);
		return;
	}

	if (fseek(stream, 0, SEEK_SET))
	{
		fprintf(stderr, "fseek(stream, offset, SEEK_SET)");
		fclose(stream);
		return;
	}

	if ((err = (fread(&total_of_obj, sizeof(size_t), 1, stream))) != 1)
	{
		fprintf(stderr, "ERROR: fread %s", path);
		fclose(stream);
		return;
	}

	for (; total_of_obj > 0; total_of_obj--)
	{
		address = new Address();
		if ((err = (fread(address, (size_t)sizeof(Address), 1, stream))) != 1)
		{
			fprintf(stderr, "ERROR: fread %s", path);
			fclose(stream);
			return;
		}
		d_array.push_back(address);
	}
	fclose(stream);
}


char* file_path(void)
{
	const int length_of_path = 256;
	char* path;
	char ch;
	int i;
	errno_t err;

	path = (char*)malloc((size_t)sizeof(char) * (length_of_path + 1));

	if (path == NULL)
	{
		fprintf(stderr, "ERROR: char* path = malloc((size_t)sizeof(char) + 1)");
		exit(EXIT_FAILURE);

	}
	printf("Enter name of file, for example C:\\Users\\User\\Documents\\File.db :");
	for (i = 0; i < length_of_path; i++)
	{
		err = scanf_s("%c", &ch, (unsigned int)sizeof(char));
		if (err == EOF || err == 0 || ch == '\n')
			break;
		path[i] = ch;
	}
	path[i] = '\0';

	return path;
}


void print(void)
{
	int i;
	Address* address;

	cout << "\n****************************\n";
	for (i = 0; i < d_array.size(); i++)
	{
		address = d_array[i];
		address->print();
		cout << "\n";
	}
	cout << "\n****************************\n";
	cout << "TOTAL:";
	cout << i << endl;
	cout << '\n';
}

/*
* Testing created clases
*/
void test(void)
{
	//**************************** BEGIN TESTING **********************************
	char c;
	cout << "TEST!!!" << endl;
	Address* a1 = new Address();
	a1->print();
	cout << "\n";

	Address* a2 = new Address("United States", "Cambridge", "Massachusetts Ave", 77);
	a2->print();
	cout << "\n";

	Address* a3 = new Address("United States", "Berkeley", "Carleton St", 2000, 1, "Christopher Hunn", 1, 44);
	a3->print();
	cout << "\n";

	Address* b1 = new Address();
	b1->set_country("United States");
	b1->set_city("Redmond");
	b1->set_street("Microsoft Way");
	b1->set_house_number(1);
	b1->set_person_number(1);
	b1->set_full_name("Bill Gates");
	b1->set_gender(1);
	b1->set_age(68);
	b1->print();
	cout << "\n";

	delete a1;
	delete a2;
	delete a3;
	delete b1;

	cout << "\n\n";

	a1 = NULL;
	a2 = NULL;
	a3 = NULL;
	b1 = NULL;

	Address a4;
	a4.print();
	cout << "\n";

	Address a5("United States", "Cambridge", "Massachusetts Ave", 77);
	a5.print();
	cout << "\n";

	Address a6("United States", "Berkeley", "Carleton St", 2000, 1, "Christopher Hunn", 1, 44);
	a6.print();
	cout << "\n";

	Address b2;
	b2.set_country("United States");
	b2.set_city("Redmond");
	b2.set_street("Microsoft Way");
	b2.set_house_number(1);
	b2.set_person_number(1);
	b2.set_full_name("Bill Gates");
	b2.set_gender(1);
	b2.set_age(68);
	b2.print();
	cout << "\n";

	cout << "Press any key" << endl;
	cin >> c;
	cout << "\n";

	//**************************** END TESTING ************************************
}