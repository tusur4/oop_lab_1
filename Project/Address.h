/**
* This class creates a person's address
*/
#pragma once
#include "my_string.h"
#include "my_vector.h"
#include <iostream> 
#include <stdio.h>	


using namespace std;

class Address
{
private:
	char name[LENGTH_STRING];		//Full address including country, city, street and house number.
	char country[LENGTH_STRING];	//Country name in an address.
	char city[LENGTH_STRING];		//City name in an address.
	char street[LENGTH_STRING];		//Street name in an address.
	int house_number;				//House number in an address.
	void assemble_address(void);	//Collect full address.
	int person_number;				//Person identification code in a database.
	char full_name[LENGTH_STRING];	//Full name of person.
	bool gender;					//Gender of person.
	int age;						//Age of person.

public:
	//**************************Constructors**************************************

	/*
	* Initialise class without parameters.
	* Default country The country of address: "Russian Federation".
	* Default city The city in the country: "".
	* Default street The steet in the city: ".
	* Default house_number The house number on the steet: 0.
	* Default person_number Person identification code in the database: 0.
	* Default full_name Last name + middle name / patronymic + first name: "".
	* Default gender Gender: 0.
	* Default age Age: 0.
	*/
	Address();


	/*
	* Initialise class 
	* Default person_number Person identification code in the database: 0.
	* Default full_name First name, patronymic / middle name and last name: "".
	* Default gender Gender: 0.
	* Default age Age: 0.
	* @param country The country of address	.
	* @param city The city in the country.
	* @param street The steet in the city.
	* @param house_number The house number on the steet. It must be greater than or equal to zero.
	*/
	Address(const char* country, const char* city, const  char* street, int house_number);


	/*
	* Initialise class.
	* @param country The country of address.
	* @param city The city in the country.
	* @param street The steet in the city.
	* @param house_number The house number on the steet. Greater than zero.
	* @param person_number Person identification code in the database.
	* @param full_name Full name.
	* @param gender Gender 1 - man, 0 - woman.
	* @param age Age. Greater or equal than or equal to zero .
	*/
	Address(const char* country, const char* city, const char* street, int house_number, int person_number, const char* full_name, bool gender, int age);


	//**************************Destructor****************************************

	/*
	* Destructor. Clear memory.
	*/
	~Address();

	//**************************Setters*******************************************

	/*
	* Set country name in an address.
	* @param name Country name. NULL means no data to write
	*/
	void set_country(const char* name);

	/*
	* Set city name in an address.
	* @param city City name. NULL means no data to write
	*/
	void set_city(const char* city);

	/*
	* Set street name in an address.
	* @param street Street name. NULL means no data to write
	*/
	void set_street(const char* street);

	/*
	* Set house number name in an address. 
	* @param house_number House number. It must be greater than or equal to zero.
	* @return True if the number is greater than or equal to zero. False otherwise.
	*/
	bool set_house_number(int house_number);

	/*
	* Set person identification code in the database.
	* @param person_number Person identification code. It must be greater than or equal to zero.
	* @return True if the person number is greater than or equal to zero. False otherwise.
	*/
	bool set_person_number(int person_number);

	/*
	* Set full name.
	* @param full_name Full name. NULL means no data to write
	*/
	void set_full_name(const char* full_name);

	/*
	* Set gender. 
	* @param gender Gender 1 - man, 0 - woman.
	*/
	void  set_gender(bool gender);

	/*
	* Set age.
	* @param age Age. It must be greater or equal than  zero.
	* @return True if an age is greater than or equal to zero. False otherwise.
	*/
	bool  set_age(int age);


	//****************************Getters********************************************

	/*
	* Get country by address.
	* @return Country.
	*/
	const char* get_country(void) const;

	/*
	* Get city by address.
	* @return City.
	*/
	const char* get_city(void) const;

	/*
	* Get steet by address.
	* @return Steet name.
	*/
	const char* get_street(void) const;

	/*
	* Get house number by address.
	* @return House number.
	*/
	int get_house_number(void) const;

	/*
	* Get full address.
	* @return Full address. Country, city, steet, house number.
	*/
	const char* get_address(void) const;

	/*
	* Get person identification cod.
	* @return Person identification cod.
	*/
	int	  get_person_number(void) const;

	/*
	* Get full name.
	* @return Full name.
	*/
	const char* get_full_name(void)  const;

	/*
	* Get gender.
	* @return Gender. 1 - man, 0 - woman.
	*/
	 bool  get_gender(void) const;

	/*
	* Get age. 
	* @return Age.
	*/
	int  get_age(void) const;

	//****************************Others********************************************

	/*
	* Prints address and contact information.
	*/
	void print(void) const;

	/*
	* Enter data from the console.
	* @param set_person_number True if it is necessary to set person's number, otherwise false.
	* @param addresses Vector of addresses.
	*/
	void input(bool set_person_number, vector < Address*> addresses);
};

