/**
* Additional features  for vector
*/

#pragma once
#include "my_vector.h"

int seach_address_in_vector_by_number(vector <class Address*> adresses, int number)
{
	for (int i = 0; i < adresses.size(); i++) {
		if (adresses[i]->get_person_number() == number)
			return i;
	}
	return -1;
}