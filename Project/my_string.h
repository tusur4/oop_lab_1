/**
* Extended string functions
*/

#pragma once
#include <corecrt.h>
#include <cstdlib>
#include <cstring>
#include <stdio.h>
#include <iostream> 

#define LENGTH_STRING 256

using namespace std;

/*
* This function displays a message to the user and terminates the program.
* @param message The message for user
*/
void terminate(const char* message);

/*
* This function concatenates two strings.
* @param dest String is the destination
* @param destsz Size
* @param src String is the source
*/
void my_strcat(char* dest, rsize_t destsz, const char* src);

/*
* This function copies two strings.
* @param dest String is the destination
* @param destsz Size
* @param src String is the source
*/
void my_strcpy(char* dest, rsize_t destsz, const char* src);

/*
* Get string from input.
* @param dest String is the destination
* @param destsz Size
*/
void my_getline(char* dest, rsize_t destsz);

/*
* Get an integer number from input, discarding other characters 
* @return The result.
*/
int my_get_int(void);

