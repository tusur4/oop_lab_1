/**
* Extended vector functions
*/

#pragma once
#include <vector>
#include "Address.h"

using namespace std;

/*
* Searchs for an address in the vector
 *@param adresses The vector.
* @param number The person number to search.
* @return Returns -1 if not found, or element's index otherwise
*/
int seach_address_in_vector_by_number(vector <class Address*> adresses, int number);