#include <ctype.h>
#include "my_string.h"


errno_t err;

void terminate(const char* message)
{
	cout << "\n" << message;
	exit(EXIT_FAILURE);
}

void my_strcat(char* dest, rsize_t destsz, const char* src)
{
	err = strcat_s(dest, destsz, src);

	if (err)
	{
		terminate("ERROR in my_strcat");
	}

}

void my_strcpy(char* dest, rsize_t destsz, const char* src)
{
	err = strcpy_s(dest, destsz, src);

	if (err)
	{
		terminate("ERROR in my_strcat");
	}

}

void my_getline(char* dest, rsize_t destsz)
{
	char ch;
	int i;

	for (i = 0; i < destsz; i++)
	{
		ch = getchar();
		if (ch == '\n' && !i)	//skip line break
			ch = getchar();
		if (ch == '\n')	//end of line
			break;
		dest[i] = ch;
	}

	dest[i] = '\0';
}

int my_get_int(void)
{
	int i;
	int num;
	int negative;
	char ch;

	for (i = 0, num = 0, negative = 1; ; i++)
	{
		ch = tolower(getchar());
		if (ch == '\n')
			break;
		if (!i && ch == 45)
		{
			negative = -1;
			continue;
		}

		if (ch < 48 || ch > 57)
			continue;
		num = num * 10 + (ch - 48);

	}

	return num * negative;
}