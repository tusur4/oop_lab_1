#include "Address.h"

const char void_str[] = "";		//The template of empty string

Address::Address()
{
	this->country[0] = '\0';
	this->city[0] = '\0';
	this->street[0] = '\0';
	this->name[0] = '\0';

	char init_country[] = { "Russian Federation" };

	my_strcpy(this->country, LENGTH_STRING, init_country);
	house_number = 0;
	this->full_name[0] = '\0';
	this->person_number = 0;
	this->gender = false;
	this->age = 0;


	assemble_address();

}

Address::Address(const char* country, const char* city, const char* street, int house_number) : Address()
{
	my_strcpy(this->country, LENGTH_STRING, country);
	my_strcpy(this->city, LENGTH_STRING, city);
	my_strcpy(this->street, LENGTH_STRING, street);

	this->house_number = house_number;

	assemble_address();
}

Address::Address(const char* country, const char* city, const  char* street, int house_number, int person_number, const char* full_name, bool gender, int age) :Address(country, city, street, house_number)
{
	this->person_number = person_number;
	this->full_name[0] = '\0';
	my_strcpy(this->full_name, LENGTH_STRING, full_name);
	this->gender = gender;
	this->age = age;

	assemble_address();
}

Address::~Address()
{
	cout << "\nAddress. Destructor executed";
}


void Address::assemble_address(void)
{
	int i;
	char* str_house_number{};
	char buffer[10];
	char space[] = { ", " };
	char dot[] = { "." };
	name[0] = '\0';


	my_strcat(this->name, LENGTH_STRING, country);
	my_strcat(this->name, LENGTH_STRING, space);

	my_strcat(this->name, LENGTH_STRING, city);
	my_strcat(this->name, LENGTH_STRING, space);

	my_strcat(this->name, LENGTH_STRING, street);
	my_strcat(this->name, LENGTH_STRING, space);


	i = sprintf_s(buffer, "%d", house_number);
	if (i <= 0)
		terminate("ERROR in sprintf_s");

	my_strcat(this->name, LENGTH_STRING, buffer);
	my_strcat(this->name, LENGTH_STRING, dot);
}

void Address::set_country(const char* country)
{
	if (!country)
		return;

	my_strcpy(this->country, LENGTH_STRING, country);
	Address::assemble_address();
}

void Address::set_city(const char* city)
{
	if (!city)
		return;

	my_strcpy(this->city, LENGTH_STRING, city);
	Address::assemble_address();
}

void Address::set_street(const char* street)
{
	if (!street)
		return;

	my_strcpy(this->street, LENGTH_STRING, street);
	Address::assemble_address();
}

bool Address::set_house_number(int house_number)
{
	if (house_number < 0)
		return false;

	this->house_number = house_number;
	Address::assemble_address();
	return true;
}

bool Address::set_person_number(int person_number)
{
	if (person_number < 0)
		return false;

	this->person_number = person_number;
	return true;
}

void Address::set_full_name(const char* full_name)
{
	if (!full_name)
		return;

	my_strcpy(this->full_name, LENGTH_STRING, full_name);
}

void Address::set_gender(bool gender)
{
	this->gender = gender;
}

bool Address::set_age(int age)
{
	if (age < 0)
		return false;

	this->age = age;
	return true;
}

const char* Address::get_country(void) const
{
	return  this->country;
}

const char* Address::get_city(void) const
{
	return this->city;
}

const char* Address::get_street(void) const
{
	return this->street;
}

int Address::get_house_number(void) const
{
	return this->house_number;
}

const char* Address::get_address(void) const
{
	return this->name;
}

int	 Address::get_person_number(void) const
{
	return this->person_number;
}

const char* Address::get_full_name(void) const
{
	return this->full_name;
}

bool Address::get_gender(void) const
{
	return this->gender;
}

int Address::get_age(void)const
{
	return this->age;
}

void Address::print(void) const
{
	cout << this->person_number;
	cout << " " << this->full_name << " ";
	if (this->gender)
		cout << "man";
	else
		cout << "man" "woman";
	cout << " " << this->age;
	cout << " " << this->name;
}

void Address::input(bool set_person_number, vector < Address*> addresses)
{
	int t_person_number;
	char t_full_name[LENGTH_STRING];
	bool t_gender;
	int temp;
	int t_age;

	char t_country[LENGTH_STRING];
	char t_city[LENGTH_STRING];
	char t_street[LENGTH_STRING];
	int t_house_number;


	if (set_person_number)
	{
		for (;;)
		{
			cout << "\nEnter person number:";
			t_person_number = my_get_int();

			if (t_person_number > 0)
			{
				if (seach_address_in_vector_by_number(addresses, t_person_number) >= 0)
				{
					cout << "This number exists, please type another number\n";
					continue;
				}
				break;
			}
			else
				cout << "\nThis is a required field. You have typed in an invalid number!\n";
		}
	}

	cout << "Enter full name:";
	my_getline(t_full_name, LENGTH_STRING);

	cout << "Enter genter 1- man, 0 - woman:";
	for (;;)
	{
		temp = my_get_int();
		if (temp  == 1)
		{
			t_gender = true;
			break;
		}
		else if (temp  == 0) {
			t_gender = false;
			break;
		}
		else
		{
			cout << "\nPlease enter the correct gender:";
		}
	}

	cout << "Enter an age:";
	for (;;)
	{
		if ((t_age = my_get_int()) >= 0)
			break;
		else
			cout << "\nAn age can not be negative. Enter an age:";
	}

	cout << "Enter a country:";
	my_getline(t_country, LENGTH_STRING);

	cout << "Enter a city : ";
	my_getline(t_city, LENGTH_STRING);

	cout << "Enter a street:";
	my_getline(t_street, LENGTH_STRING);

	cout << "Enter a house number:";
	for (;;)
	{
		if ((t_house_number = my_get_int()) >= 0)
			break;
		else
			cout << "\nThe house number can not be negative. Enter a house number:";
	}

	cout << "\n";

	if (set_person_number)
	{
		this->set_person_number(t_person_number);
	}
	this->set_full_name(t_full_name);
	this->set_gender(t_gender);
	this->set_age(t_age);
	if (t_country[0] != '\0')
		this->set_country(t_country);
	if (t_city[0] != '\0')
		this->set_city(t_city);
	if (t_street[0] != '\0')
		this->set_street(t_street);
	this->set_house_number(t_house_number);

}

